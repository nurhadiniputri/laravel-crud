@extends('layout.master')
@section('judul')
    <h1>Halaman Welcome</h1>
@endsection
@section('subjudul')
    <h2 class="card-title">SELAMAT DATANG {{$depan}} {{$belakang}}</h2>
@endsection
@section('isi')
    <p>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</p>
@endsection
