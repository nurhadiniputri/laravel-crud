<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('register');
    }
    public function welcome(Request $request){
        $depan = strtoupper($request->firstname);
        $belakang = strtoupper($request->lastname);
        return view ('welcome',compact('depan','belakang'));
    }
}
