@extends('layout.master')
@section('judul')
    <h1>CRUD Cast</h1>
@endsection
@section('subjudul')
    <h1 class="card-title"><b>Detail Data Cast</b></h1>
@endsection
@section('isi')
    <h4>Show Cast dengan Id {{$cast->id}}</h4><br>
    <p>Nama : {{$cast->nama}}</p>
    <p>Umur : {{$cast->umur}} tahun</p>
    <p>Bio : {{$cast->bio}}</p>
@endsection