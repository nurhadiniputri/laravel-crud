@extends('layout.master')
@section('judul')
    <h1>CRUD Cast</h1>
@endsection
@section('subjudul')
    <h1 class="card-title"><b>Edit Data Cast</b></h1>
@endsection
@section('isi')
<div>
        <form action="/cast/{{$cast->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Masukkan bio">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
